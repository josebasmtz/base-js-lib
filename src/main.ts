
const version = '1.0.0';

const exportable = {
    version
}

export default exportable;